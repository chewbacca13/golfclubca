import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class Field extends JPanel {

    private ArrayList<Ball> balls;
    private ArrayList<Hole> holes;
    private ArrayList<Path> paths;

    private Field(int n) {
        balls = new ArrayList<>(n);
        holes = new ArrayList<>(n);
        for(int i=0;i<n;i++)
        {
            balls.add(new Ball(900));
            holes.add(new Hole(900));
        }
        paths = new ArrayList<>(n);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        this.setBackground(Color.WHITE);
        g.setColor(new Color(1,122,21));
        g.fillRect(5,5,900,900);
        g.fillRect(910,5,900,900);
        g.setColor(Color.WHITE);
        for(Ball ball:this.balls)
        {
            g.fillOval((int)ball.getX(),900-(int)ball.getY(),10,10);
            g.fillOval((int)ball.getX()+905,900-(int)ball.getY(),10,10);
        }
        g.setColor(Color.BLACK);
        for(Hole hole:this.holes)
        {
            g.fillOval((int)hole.getX(),900-(int)hole.getY(),10,10);
            g.fillOval((int)hole.getX()+905,900-(int)hole.getY(),10,10);
        }
        g.setColor(Color.RED);
        for(Path path:this.paths)
        {
            g.drawLine((int)path.getBall().getX()+910,905-(int)path.getBall().getY(),(int)path.getHole().getX()+910,905-(int)path.getHole().getY());
        }

    }

    public static void main(String[] args) throws IOException {
        System.out.println("Please give N for the number of balls followed by pressing ENTER!");
        int n = 50; // = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
        Field field = new Field(n);
        solvePaths(field.balls,field.holes,field.paths);
        System.out.println(field);
        JFrame jFrame = new JFrame("Golf");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(field);
        jFrame.setSize(1832,948);
        jFrame.setVisible(true);
    }

    private static void solvePaths(ArrayList<Ball> balls, ArrayList<Hole> holes, ArrayList<Path> paths) {
        if(balls.size()>0)
        {
            for(int i=0;i<balls.size();i++)
                for(int j = 0; j < holes.size(); j++)
                {
                    ArrayList<Ball> ballsCopy=new ArrayList<>(balls);
                    ArrayList<Hole> holesCopy=new ArrayList<>(holes);

                    Ball currentBall = ballsCopy.remove(i);
                    Hole currentHole = holesCopy.remove(j);
                    try {

                        SplitField splitField = new SplitField(currentBall,currentHole,ballsCopy,holesCopy);
                        if(splitField.splitsEvenly())
                        {
                            paths.add(new Path(currentBall,currentHole));
                            solvePaths(splitField.aboveBalls,splitField.aboveHoles,paths);
                            solvePaths(splitField.belowBalls,splitField.belowHoles,paths);
                            return;
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        System.out.println(j);
                    }
                }
        }

    }

    @Override
    public String toString() {
        return "Field{" +
                "paths=" + paths +
                '}';
    }
}

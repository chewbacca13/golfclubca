public class Path {
    private Ball ball;
    private Hole hole;

    Path(Ball ball, Hole hole) {
        this.ball = ball;
        this.hole = hole;
    }

    public Ball getBall() {
        return ball;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public Hole getHole() {
        return hole;
    }

    public void setHole(Hole hole) {
        this.hole = hole;
    }

    @Override
    public String toString() {
        return "Path{" +
                "ball=" + ball +
                ", hole=" + hole +
                '}';
    }
}

import java.util.ArrayList;

class SplitField {
    ArrayList<Ball> aboveBalls;
    ArrayList<Hole> aboveHoles;
    ArrayList<Ball> belowBalls;
    ArrayList<Hole> belowHoles;

    SplitField(Ball ball, Hole hole, ArrayList<Ball> balls, ArrayList<Hole> holes) throws Exception {
        double x = ball.getX()-hole.getX();
        double y = ball.getY()-hole.getY();
        double m = y/x;
        double b = ball.getY()-(m*ball.getX());
        aboveBalls = new ArrayList<>();
        aboveHoles = new ArrayList<>();
        belowBalls = new ArrayList<>();
        belowHoles = new ArrayList<>();
        for (int i=0; i<balls.size();i++)
        {
            if (balls.get(i).getY()>m*balls.get(i).getX()+b)
            {
                aboveBalls.add(balls.get(i));
            }
            else
            {
                if (balls.get(i).getY()<m*balls.get(i).getX()+b)
                {
                    belowBalls.add(balls.get(i));
                }
                else
                {
                    throw new Exception("There are 3 collinear points");
                }
            }
            if (holes.get(i).getY()>m*holes.get(i).getX()+b)
            {
                aboveHoles.add(holes.get(i));
            }
            else
            {
                if(holes.get(i).getY()<m*holes.get(i).getX()+b)
                {
                    belowHoles.add(holes.get(i));
                }
                else
                {
                    throw new Exception("There are 3 collinear points");
                }
            }
        }
    }

    Boolean splitsEvenly()
    {
        return aboveBalls.size()==aboveHoles.size() && belowBalls.size()==belowHoles.size();
    }
}

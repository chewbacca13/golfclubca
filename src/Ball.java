import java.util.Random;

public class Ball {
    private double x;
    private double y;

    Ball(double maxValue) {
        Random random = new Random();
        this.x=random.nextDouble()*maxValue;
        this.y=random.nextDouble()*maxValue;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Ball{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
